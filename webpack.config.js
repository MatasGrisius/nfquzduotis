var path = require('path');
var webpack = require('webpack');
 
module.exports = {
  entry: './scripts/index.jsx',
  output: { 
    path: __dirname + "/content", 
    filename: 'bundle.js' 
  },
  module: {
    loaders: [
      {
        test: /.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
        query: {
          presets: ['es2015', 'react']
        }
      }
    ]
  },
  devServer: {
    contentBase: './content',
    hot: true,
    historyApiFallback: true,
  }
};