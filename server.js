var express = require('express');
var app = express();
var path = require('path');

app.use(express.static(path.join(__dirname, 'content')));

app.use('*',(req,res)=>{
    res.sendFile(path.resolve(__dirname+'/content/index.html')) 
})


app.listen(process.env.PORT || 8080, function(){
    console.log((new Date()) + " Server is listening on port " + (process.env.PORT || 8080));
});