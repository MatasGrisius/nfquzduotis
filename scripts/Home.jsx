import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';

class Home extends React.Component {
    constructor(props) {
      super(props);

      this.state = {}
    };

    render() {
        return (
            <div className="center">
                <h1>Welcome</h1>
                <h2>Thank you for choosing our shop</h2>
                <Link to='/products' >
                    <img src="https://www.budgettravel.ie/assets/userfiles/holiday-offers-croatia.jpg" />
                </Link>
            </div>
        )
    }
}

export default Home;