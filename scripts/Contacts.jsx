import React from 'react';
import ReactDOM from 'react-dom';

class Contacts extends React.Component {
    constructor(props) {
      super(props);

      this.state = {}
    };

    render() {
        return (
            <div className="center">
                <h1>Contacts</h1>
                <h4>ShopShop</h4>
                <h4>Veiverių street 51A, Kaunas,</h4>
                <h4>LT-46336</h4>
                <h4>Matas Grišius</h4>
                <img src="https://i.imgur.com/uOBdC1t.png" />
            </div>
        )
    }
}

export default Contacts;