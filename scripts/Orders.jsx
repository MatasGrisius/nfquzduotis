import React from 'react';
import Pagination from './utils/Pagination.jsx';
import orders from '../data/orders-list.json';
 
class Orders extends React.Component {
    constructor() {
        super();

        if (window.newOrders) {
            window.newOrders.forEach(element => {
                orders.push(element);
            });
        }
        window.newOrders = [];

        this.state = {
            allItems: orders.sort((a,b) => {
                let aDate = new Date(a.date);
                aDate.setHours(a.time.split(':')[0],a.time.split(':')[1]);
                let bDate = new Date(b.date);
                bDate.setHours(b.time.split(':')[0],b.time.split(':')[1]);
                return aDate > bDate ? -1 : aDate<bDate ? 1 : 0;
            }),
            filteredItems: orders,
            pageOfItems: []
        };
 
        // bind function in constructor instead of render (https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-no-bind.md)
        this.onChangePage = this.onChangePage.bind(this);
    }

    handleSearch(event) {
        this.setState({
            filteredItems: this.state.allItems.filter(i => Object.keys(i).some(key => i[key].toString().indexOf(event.target.value) > -1)),
        });
    };
 
    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }    
 
    render() {
        return (
            <div className="center">
                <h4> Search: </h4>
                <input onInput={this.handleSearch.bind(this)}/>
                <hr/>
                <h1>Orders list:</h1>
                <div className="container">
                    <div className="text-center">
                        <table id="customers">
                            <tbody>
                                <tr>
                                    <th>Name</th>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Order Date</th>
                                    <th>Address</th>
                                </tr>
                                {this.state.pageOfItems.map((item, i) =>
                                    <tr key={i}>
                                        <td>{item.last_name + " " + item.first_name}</td>
                                        <td>{item.product}</td>
                                        <td>{item.price}</td>
                                        <td>{item.date + " " + item.time}</td>
                                        <td>{item.address + ", " + item.city}</td>
                                    </tr>
                                )}
                            </tbody>
                        </table>
                        <Pagination items={this.state.filteredItems} onChangePage={this.onChangePage} />
                    </div>
                </div>
            </div>
        );
    }
}
 
export default Orders;