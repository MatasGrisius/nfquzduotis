import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, NavLink, Switch, Route } from 'react-router-dom';
import Home from "./Home.jsx";
import Products from "./Products.jsx";
import Orders from "./Orders.jsx";
import Contacts from "./Contacts.jsx";

ReactDOM.render(
	<BrowserRouter>
		<div>
			<div className="repo"><a href="https://bitbucket.org/MatasGrisius/nfquzduotis">bitbucket.org/MatasGrisius/nfquzduotis</a></div>
			<header>
				<nav>
				<ul>
					<li><NavLink activeClassName="active" exact to='/'>Home</NavLink></li>
					<li><NavLink activeClassName="active" to='/products'>Products</NavLink></li>
					<li><NavLink activeClassName="active" to='/orders'>Orders</NavLink></li>
					<li><NavLink activeClassName="active" to='/contacts'>Contacts</NavLink></li>
				</ul>
				</nav>
			</header>
			<Switch>
				<Route exact path='/' component={Home}/>
				<Route path='/products' component={Products}/>
				<Route path='/orders' component={Orders}/>
				<Route path='/contacts' component={Contacts}/>
			</Switch>
			<hr className="footer" />
			<p className="footer">nfq-uzduotis2018.heroku.com</p>
		</div>
  </BrowserRouter>,
	document.querySelector('#app')
);