import React from 'react';
import ReactDOM from 'react-dom';
import swal from 'sweetalert2';
import products from '../data/products-list.json';

class Products extends React.Component {
    constructor(props) {
        super(props);

        if (!window.newOrders) {
            window.newOrders = [];
        }

        this.state = {
            allItems: products,
            filteredItems: products
        }
    };

    handleSearch(event) {
        this.setState({
            filteredItems: this.state.allItems.filter(i => !event.target.value || i.name.toLowerCase().indexOf(event.target.value.toLowerCase()) > -1)
        });
    };

    handleBuy(itemId) {
        swal({
            title: 'Are you sure you want to buy?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, buy tickets',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            reverseButtons: true
          }).then((result) => {
            if (result.value) {
              swal(
                'Bought!',
                'Your tickets were bought.',
                'success'
              );
              let now = new Date();
              window.newOrders.push({
                    id: 1000,
                    first_name: "Matas",
                    last_name: "Grišius",
                    product: products[itemId].name,
                    price: products[itemId].price,
                    date: '' + (1+now.getMonth()) + '/' + now.getDate() + '/' + now.getUTCFullYear(),
                    time: '' + now.getHours() + ':' + ('0'+now.getMinutes()).slice(-2),
                    city: "Kaunas",
                    address: "16, V. Kreves g."
                })
            }
          })
    }

    render() {
        return (
            <div className="center">
                <h4> Search: </h4>
                    <input onInput={this.handleSearch.bind(this)}/>
                    <hr/>
               <h1>Products</h1>
               {this.state.filteredItems.map((i) => 
                <div className="card inline" key={i.id}>
                    <img src={i.image} alt="Avatar" style={{width:"100%", height:"150px"}}/>
                    <div className="container" style={{position: "relative", width:"100%", height: "150px"}} >
                        <h4><b>{i.name}</b></h4> 
                        <p>from €{i.price}</p> 
                        <div className="buyButton btn btn-primary" type="button" onClick={() => this.handleBuy(i.id)}>
                            <span className="glyphicon glyphicon-shopping-cart"/> Buy tickets
                        </div>
                    </div>
                </div>
               )}
               <div className="clear-inline"></div>
            </div>
        )
    }
}

export default Products;
